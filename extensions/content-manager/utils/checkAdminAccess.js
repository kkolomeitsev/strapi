function parseUserPermissionsRoutes(modelObj) {
  if (modelObj.controller === "role") {
    switch (modelObj.action) {
      case "findOne":
        modelObj.action = "getrole";
        break;
      case "find":
        modelObj.action = "getroles";
        break;
      case "create":
        modelObj.action = "createrole";
        break;
      case "update":
        modelObj.action = "updaterole";
        break;
      case "destroy":
        modelObj.action = "deleterole";
        break;
    }

    modelObj.controller = "userspermissions";
  }
}

function parseModel(model) {
  if (model.startsWith("plugins::")) {
    const [, path] = model.split("::");
    const [plugin, controller, action] = path.split(".");

    return { plugin, controller, action };
  }

  if (model.startsWith("application::")) {
    const [, path] = model.split("::");
    const [api, controller, action] = path.split(".");
    return { api, controller, action };
  }

  return null;
}

async function getAdmin(ctx) {
  const adminTokenData = await strapi.plugins[
    "users-permissions"
  ].services.jwt.getToken(ctx);

  const adminData =
    adminTokenData && adminTokenData.id
      ? await strapi
          .query("administrator", "admin")
          .findOne({ id: adminTokenData.id })
      : null;

  return adminData;
}

async function getAdminRoles(username) {
  const contentManagerService =
    strapi.plugins["content-manager"].services.contentmanager;

  const rolesData = await contentManagerService.fetchAll(
    {
      model: "application::admin-roles.admin-roles",
    },
    {}
  );

  const roles = [];

  if (rolesData && rolesData.length) {
    rolesData.forEach((roleDoc) => {
      if (Array.isArray(roleDoc.admins) && roleDoc.admins.includes(username)) {
        if (roleDoc.role) {
          roles.push(roleDoc.role.id);
        }
      }
    });
  }

  return roles;
}

async function getAdminProjects(username) {
  const contentManagerService =
    strapi.plugins["content-manager"].services.contentmanager;

  const projectsData = await contentManagerService.fetchAll(
    {
      model: "application::projects-division.projects-division",
    },
    {}
  );

  const projects = [];

  if (projectsData && projectsData.length) {
    projectsData.forEach((projectDoc) => {
      if (
        Array.isArray(projectDoc.admins) &&
        projectDoc.admins.includes(username)
      ) {
        projects.push(projectDoc.id);
      }
    });
  }

  return projects;
}

async function checkAdminProjects(ctx) {
  try {
    const adminData = await getAdmin(ctx);

    if (!adminData) {
      return null;
    }

    if (
      Array.isArray(strapi.config.serviceAdmins) &&
      strapi.config.serviceAdmins.includes(adminData.username)
    ) {
      return "*";
    }

    const projects = await getAdminProjects(adminData.username);

    if (!projects.length) {
      return null;
    }

    return projects;
  } catch (e) {}

  return null;
}

async function getAdminProjectsFilter(ctx, model, filter = {}) {
  const contentType = strapi.contentTypes[model];

  if (
    model === "application::projects-division.projects-division" ||
    (contentType && contentType.attributes && contentType.attributes.project)
  ) {
    let projectFilter = null;

    const adminProjects = await checkAdminProjects(ctx);

    if (adminProjects !== "*") {
      if (adminProjects.length) {
        projectFilter = adminProjects;
      }

      if (model === "application::projects-division.projects-division") {
        filter.id = projectFilter || 0;
      } else {
        if (Array.isArray(projectFilter)) {
          projectFilter.push(null);
        }

        filter.project = projectFilter;
      }
    }
  }

  return filter;
}

async function hasFullRights(ctx) {
  const adminData = await getAdmin(ctx);

  if (!adminData) {
    return false;
  }

  if (
    Array.isArray(strapi.config.serviceAdmins) &&
    strapi.config.serviceAdmins.includes(adminData.username)
  ) {
    return true;
  }

  return false;
}

async function getAvailableContentActions(ctx) {
  const adminData = await getAdmin(ctx);

  if (!adminData) {
    return [];
  }

  if (
    Array.isArray(strapi.config.serviceAdmins) &&
    strapi.config.serviceAdmins.includes(adminData.username)
  ) {
    return "*";
  }

  const roles = await getAdminRoles(adminData.username);

  if (!roles.length) {
    return [];
  }

  const permissions = await strapi
    .query("permission", "users-permissions")
    .find({
      role: roles,
      enabled: true,
    });

  const availableContentActons = permissions.reduce((prev, val) => {
    if (val.type === "application") {
      const uid = `${val.controller}.${val.action}`;
      if (!prev.includes(uid)) {
        prev.push(uid);
      }
    }
    return prev;
  }, []);

  return availableContentActons;
}

async function getAvailableContentTypes(ctx) {
  const adminData = await getAdmin(ctx);

  if (!adminData) {
    return false;
  }

  if (
    Array.isArray(strapi.config.serviceAdmins) &&
    strapi.config.serviceAdmins.includes(adminData.username)
  ) {
    return "*";
  }

  const roles = await getAdminRoles(adminData.username);

  if (!roles.length) {
    return false;
  }

  const permissions = await strapi
    .query("permission", "users-permissions")
    .find({
      role: roles,
      enabled: true,
    });

  const permissionContentTypes = permissions.reduce((prev, val) => {
    if (val.type === "application") {
      const uid = `application::${val.controller}`;
      if (uid !== "application::projects-division") {
        if (!prev.includes(uid)) {
          prev.push(uid);
        }
      }
    } else {
      let uid = `plugins::${val.type}.${val.controller}`;
      if (val.type === "users-permissions") {
        if (val.controller === "userspermissions") {
          if (val.action === "getroles") {
            uid = `plugins::${val.type}.role`;
          } else if (val.action === "getpermissions") {
            uid = `plugins::${val.type}.permission`;
          } else {
            uid = false;
          }
        } else if (val.controller === "user") {
          if (val.action === "find") {
            uid = `plugins::${val.type}.${val.controller}`;
          } else {
            uid = false;
          }
        }
      } else if (val.type === "upload") {
        if (val.action === "find") {
          uid = `plugins::${val.type}.file`;
        } else {
          uid = false;
        }
      }
      if (uid && !prev.includes(uid)) {
        prev.push(uid);
      }
    }
    return prev;
  }, []);

  return permissionContentTypes;
}

async function checkAdminAccess(ctx) {
  try {
    const adminData = await getAdmin(ctx);

    if (!adminData) {
      return null;
    }

    if (
      Array.isArray(strapi.config.serviceAdmins) &&
      strapi.config.serviceAdmins.includes(adminData.username)
    ) {
      return true;
    }

    const roles = await getAdminRoles(adminData.username);

    if (!roles.length) {
      return null;
    }

    const route = ctx.request.route;

    const { model } = ctx.params;

    const parsedModel = model ? parseModel(model) : {};

    if (!parsedModel.action) {
      parsedModel.action = route.action;
    }

    if (parsedModel.plugin && parsedModel.plugin === "users-permissions") {
      parseUserPermissionsRoutes(parsedModel);
    }

    if (!parsedModel.plugin && !parsedModel.api && route.plugin) {
      parsedModel.plugin = route.plugin;
    }

    if (!parsedModel.controller && route.controller) {
      parsedModel.controller = route.controller;
    }

    const { plugin, controller, action } = parsedModel;

    const permission = await strapi
      .query("permission", "users-permissions")
      .findOne({
        role: roles,
        type: plugin ? plugin : "application",
        controller,
        action: !plugin
          ? action
          : plugin !== "users-permissions"
          ? action
          : action === "count"
          ? "find"
          : action,
        enabled: true,
      });

    if (permission) {
      return true;
    }
  } catch (e) {}

  return null;
}

function generateActionWrapper(action) {
  return async function (ctx) {
    const adminAccess = await checkAdminAccess(ctx);

    if (!adminAccess) {
      ctx.response.forbidden(
        "You do not have sufficient permissions to perform this operation"
      );
      return;
    }

    return await action(ctx);
  };
}

module.exports = {
  getAvailableContentTypes,
  checkAdminProjects,
  getAdminProjectsFilter,
  hasFullRights,
  getAvailableContentActions,
  wrapController({ controller, exclude, include }) {
    if (include) {
      if (Array.isArray(include)) {
        include.forEach((key) => {
          const action = controller[key];

          if (action) {
            controller[key] = generateActionWrapper(action);
          }
        });
      }
    } else {
      let hasExcludes = Array.isArray(exclude) && exclude.length ? true : false;
      Object.keys(controller).forEach((key) => {
        if (!hasExcludes || !exclude.includes(key)) {
          const action = controller[key];

          controller[key] = generateActionWrapper(action);
        }
      });
    }

    return controller;
  },
};
