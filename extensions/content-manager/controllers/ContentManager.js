"use strict";

const _ = require("lodash");

const parseMultipartBody = require("strapi-plugin-content-manager/utils/parse-multipart");
const {
  validateGenerateUIDInput,
  validateCheckUIDAvailabilityInput,
  validateUIDField,
} = require("strapi-plugin-content-manager/controllers/validation");

const {
  wrapController,
  getAdminProjectsFilter,
} = require("../utils/checkAdminAccess");

function checkEntryProjectAccess(entry, filter, ctx = false) {
  if (
    entry &&
    entry.project &&
    filter &&
    filter.project &&
    Array.isArray(filter.project)
  ) {
    const project = entry.project;

    let hasAccess = false;

    let projectId = 0;

    if (typeof project === "number") {
      projectId = project;
    } else if (typeof project === "object" && typeof project.id === "number") {
      projectId = project.id;
    }

    if (filter.project.includes(projectId)) {
      hasAccess = true;
    }

    if (!hasAccess && ctx) {
      ctx.response.forbidden(
        "You do not have sufficient permissions to perform this operation"
      );
      return;
    }

    return hasAccess;
  }

  return true;
}

const controller = {
  async generateUID(ctx) {
    const { contentTypeUID, field, data } = await validateGenerateUIDInput(
      ctx.request.body
    );

    await validateUIDField(contentTypeUID, field);

    const uidService = strapi.plugins["content-manager"].services.uid;

    ctx.body = {
      data: await uidService.generateUIDField({ contentTypeUID, field, data }),
    };
  },

  async checkUIDAvailability(ctx) {
    const {
      contentTypeUID,
      field,
      value,
    } = await validateCheckUIDAvailabilityInput(ctx.request.body);

    await validateUIDField(contentTypeUID, field);

    const uidService = strapi.plugins["content-manager"].services.uid;

    const isAvailable = await uidService.checkUIDAvailability({
      contentTypeUID,
      field,
      value,
    });

    ctx.body = {
      isAvailable,
      suggestion: !isAvailable
        ? await uidService.findUniqueUID({ contentTypeUID, field, value })
        : null,
    };
  },

  /**
   * Returns a list of entities of a content-type matching the query parameters
   */
  async find(ctx) {
    const { model } = ctx.params;

    const filter = ctx.request.query;

    await getAdminProjectsFilter(ctx, model, filter);

    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    let entities = [];
    if (_.has(ctx.request.query, "_q")) {
      entities = await contentManagerService.search({ model }, filter);
    } else {
      entities = await contentManagerService.fetchAll({ model }, filter);
    }

    ctx.body = entities;
  },

  /**
   * Returns an entity of a content type by id
   */
  async findOne(ctx) {
    const { model, id } = ctx.params;
    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    const entry = await contentManagerService.fetch({ model, id });

    // Entry not found
    if (!entry) {
      return ctx.notFound("Entry not found");
    }

    const filter = await getAdminProjectsFilter(ctx, model);

    checkEntryProjectAccess(entry, filter, ctx);

    ctx.body = entry;
  },

  /**
   * Returns a count of entities of a content type matching query parameters
   */
  async count(ctx) {
    const { model } = ctx.params;

    const filter = ctx.request.query;

    await getAdminProjectsFilter(ctx, model, filter);

    const contentType = strapi.contentTypes[model];

    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    let count;
    if (_.has(ctx.request.query, "_q")) {
      count = await contentManagerService.countSearch({ model }, filter);
    } else {
      count = await contentManagerService.count({ model }, filter);
    }

    ctx.body = {
      count: _.isNumber(count) ? count : _.toNumber(count),
    };
  },

  /**
   * Creates an entity of a content type
   */
  async create(ctx) {
    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    const { model } = ctx.params;

    const filter = await getAdminProjectsFilter(ctx, model);

    try {
      if (ctx.is("multipart")) {
        const { data, files } = parseMultipartBody(ctx);
        checkEntryProjectAccess(data, filter, ctx);
        ctx.body = await contentManagerService.create(data, { files, model });
      } else {
        // Create an entry using `queries` system
        checkEntryProjectAccess(ctx.request.body, filter, ctx);
        ctx.body = await contentManagerService.create(ctx.request.body, {
          model,
        });
      }

      await strapi.telemetry.send("didCreateFirstContentTypeEntry", { model });
    } catch (error) {
      strapi.log.error(error);
      ctx.badRequest(null, [
        {
          messages: [
            { id: error.message, message: error.message, field: error.field },
          ],
          errors: _.get(error, "data.errors"),
        },
      ]);
    }
  },

  /**
   * Updates an entity of a content type
   */
  async update(ctx) {
    const { id, model } = ctx.params;

    const filter = await getAdminProjectsFilter(ctx, model);

    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    try {
      if (ctx.is("multipart")) {
        const { data, files } = parseMultipartBody(ctx);
        checkEntryProjectAccess(data, filter, ctx);
        ctx.body = await contentManagerService.edit({ id }, data, {
          files,
          model,
        });
      } else {
        // Return the last one which is the current model.
        checkEntryProjectAccess(ctx.request.body, filter, ctx);
        ctx.body = await contentManagerService.edit({ id }, ctx.request.body, {
          model,
        });
      }
    } catch (error) {
      strapi.log.error(error);
      ctx.badRequest(null, [
        {
          messages: [
            { id: error.message, message: error.message, field: error.field },
          ],
          errors: _.get(error, "data.errors"),
        },
      ]);
    }
  },

  /**
   * Deletes one entity of a content type matching a query
   */
  async delete(ctx) {
    const { id, model } = ctx.params;
    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    const entry = await contentManagerService.fetch({ model, id });

    // Entry not found
    if (!entry) {
      return ctx.notFound("Entry not found");
    }

    const filter = await getAdminProjectsFilter(ctx, model);

    checkEntryProjectAccess(entry, filter, ctx);

    ctx.body = await contentManagerService.delete({ id, model });
  },

  /**
   * Deletes multiple entities of a content type matching a query
   */
  async deleteMany(ctx) {
    const { model } = ctx.params;
    const contentManagerService =
      strapi.plugins["content-manager"].services.contentmanager;

    const filter = ctx.request.query;

    await getAdminProjectsFilter(ctx, model, filter);

    ctx.body = await contentManagerService.deleteMany({ model }, filter);
  },
};

module.exports = wrapController({
  controller,
  exclude: ["generateUID", "checkUIDAvailability"],
});
