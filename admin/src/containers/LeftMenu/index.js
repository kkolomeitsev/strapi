/*
 *
 * LeftMenu
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { auth } from "strapi-helper-plugin";
import LeftMenuHeader from "../../components/LeftMenuHeader";
import LeftMenuLinkContainer from "../../components/LeftMenuLinkContainer";
import LeftMenuFooter from "../../components/LeftMenuFooter";
import Wrapper from "strapi-admin/admin/src/containers/LeftMenu/Wrapper";

const LeftMenu = ({ version, plugins }) => {
  const userInfo = auth.getUserInfo();

  if (!userInfo || !userInfo.fullRights) {
    if (plugins["content-type-builder"])
      plugins["content-type-builder"]["isDisplayed"] = false;
    if (plugins["email"]) plugins["email"]["isDisplayed"] = false;
    if (plugins["graphql"]) plugins["graphql"]["isDisplayed"] = false;
    if (plugins["users-permissions"])
      plugins["users-permissions"]["isDisplayed"] = false;
  }

  return (
    <Wrapper>
      <LeftMenuHeader />
      <LeftMenuLinkContainer plugins={plugins} />
      <LeftMenuFooter key="footer" version={version} />
    </Wrapper>
  );
};

LeftMenu.propTypes = {
  version: PropTypes.string.isRequired,
  plugins: PropTypes.object.isRequired,
};

export default LeftMenu;
